<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-element-salutation
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-message-element-salutation'] = array(
    'Avisota - Message element "Salutation"',
    '"Salutation" message element for Avisota messages.'
);
