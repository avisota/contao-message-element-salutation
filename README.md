[![Version](http://img.shields.io/packagist/v/avisota/contao-message-element-salutation.svg?style=flat-square)](https://packagist.org/packages/avisota/contao-message-element-salutation)
[![Stable Build Status](http://img.shields.io/travis/avisota/contao-message-element-salutation/master.svg?style=flat-square&label=stable build)](https://travis-ci.org/avisota/contao-message-element-salutation)
[![Upstream Build Status](http://img.shields.io/travis/avisota/contao-message-element-salutation/develop.svg?style=flat-square&label=dev build)](https://travis-ci.org/avisota/contao-message-element-salutation)
[![License](http://img.shields.io/packagist/l/avisota/contao-message-element-salutation.svg?style=flat-square)](https://github.com/avisota/contao-message-element-salutation/blob/master/LICENSE)
[![Downloads](http://img.shields.io/packagist/dt/avisota/contao-message-element-salutation.svg?style=flat-square)](https://packagist.org/packages/avisota/contao-message-element-salutation)

Salutation content element
==========================

for Avisota 2.
